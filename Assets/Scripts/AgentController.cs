﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentController : MonoBehaviour
{
  public float timeScale = 1f;
  public float roundTimeLeft = 0f;
  public GameObject agentPrefab;
  public Transform teamOneSpawn;
  public Transform teamTwoSpawn;
  public float mutationFactor = 0.0f;
  public float roundLength = 5f;
  public float bestFitness = -100000f;
  public int roundNr = 0;

  private List<SoldierAgent> teamOneSoldiers;
  private List<SoldierAgent> teamTwoSoldiers;
  private NeuralNetwork bestNet;
  private bool firstRun = true;
  private int teamSize = 10;

  private void Start()
  {
    teamOneSoldiers = new List<SoldierAgent>();
    teamTwoSoldiers = new List<SoldierAgent>();
    for (int i = 0; i < teamSize; i++)
    {
      GameObject instance = Instantiate(agentPrefab);
      SoldierAgent soldier = instance.GetComponent<SoldierAgent>();
      soldier.Init(teamSize);
      soldier.SetTeam(1);
      teamOneSoldiers.Add(soldier);
    }
    for (int i = 0; i < teamSize; i++)
    {
      GameObject instance = Instantiate(agentPrefab);
      SoldierAgent soldier = instance.GetComponent<SoldierAgent>();
      soldier.Init(teamSize);
      soldier.SetTeam(2);
      teamTwoSoldiers.Add(soldier);
    }
    foreach (SoldierAgent soldier in teamOneSoldiers)
    {
      soldier.SetTeamMembers(teamOneSoldiers);
      soldier.SetEnemies(teamTwoSoldiers);
    }
    foreach (SoldierAgent soldier in teamTwoSoldiers)
    {
      soldier.SetTeamMembers(teamTwoSoldiers);
      soldier.SetEnemies(teamOneSoldiers);
    }
  }

  private void FixedUpdate()
  {
    Time.timeScale = timeScale;
    roundTimeLeft -= Time.fixedDeltaTime;
    if (roundTimeLeft <= 0f || CheckForRoundResult())
    {
      if (!firstRun)
        EndRound();
      StartRound();
      firstRun = false;
    }
  }

  private bool CheckForRoundResult()
  {
    int teamOneAliveCount = 0;
    int teamTwoAliveCount = 0;
    List<SoldierAgent> allSoldiers = new List<SoldierAgent>();
    allSoldiers.AddRange(teamOneSoldiers);
    allSoldiers.AddRange(teamTwoSoldiers);
    foreach (SoldierAgent soldier in AllSoldiers())
    {
      if (soldier.IsAlive())
      {
        if (soldier.Team() == 1)
          teamOneAliveCount++;
        else if (soldier.Team() == 2)
          teamTwoAliveCount++;
      }
    }
    if (teamOneAliveCount == 0 || teamTwoAliveCount == 0)
    {
      int winningTeam = 0;
      if (teamOneAliveCount == 0)
        winningTeam = 2;
      else
        winningTeam = 1;
      foreach (SoldierAgent soldier in AllSoldiers())
      {
        if (soldier.Team() == winningTeam)
          soldier.SetFitness(soldier.Fitness() * 1.0f); // Add winning bonus here if wanted
      }
      Debug.Log("all are ded");
      return true;
    }
    return false;
  }

  private void StartRound()
  {
    roundNr++;
    roundTimeLeft = roundLength;
    SpawnSoldiers();
  }

  private void EndRound()
  {
    GenerateNewNetworks();
  }

  private void SpawnSoldiers()
  {
    foreach (SoldierAgent soldier in AllSoldiers())
    {
      int spread = 60;
      if (soldier.Team() == 1)
        soldier.Spawn(teamOneSpawn.position + new Vector3(UnityEngine.Random.Range(-spread, spread), UnityEngine.Random.Range(-spread, spread), 0));
      else
        soldier.Spawn(teamTwoSpawn.position + new Vector3(UnityEngine.Random.Range(-spread, spread), UnityEngine.Random.Range(-spread, spread), 0));
    }
  }

  private void GenerateNewNetworks()
  {
    foreach (SoldierAgent soldier in AllSoldiers())
    {
      if (soldier.Fitness() >= bestFitness)
      {
        bestFitness = soldier.Fitness();
        bestNet = soldier.Network();
      }
    }
    foreach (SoldierAgent soldier in AllSoldiers())
    {
      var resetChance = UnityEngine.Random.Range(0, 1000);
      if (resetChance < 10)
      {
        int layers = (int)UnityEngine.Random.Range(2, 5);
        int[] layerInfo = new int[layers + 2];
        layerInfo[0] = 40;
        layerInfo[layerInfo.Length - 1] = 4;
        for (int i = 1; i < layerInfo.Length - 1; i++)
        {
          layerInfo[i] = (int)UnityEngine.Random.Range(1, 20);
        }
        soldier.SetNetwork(new NeuralNetwork(layerInfo));
        continue;
      }
      NeuralNetwork net = new NeuralNetwork(bestNet);
      net.mutate(mutationFactor);
      soldier.SetNetwork(net);
    }
    AllSoldiers()[0].SetNetwork(bestNet);
  }

  private List<SoldierAgent> AllSoldiers()
  {
    List<SoldierAgent> allSoldiers = new List<SoldierAgent>();
    allSoldiers.AddRange(teamOneSoldiers);
    allSoldiers.AddRange(teamTwoSoldiers);
    return allSoldiers;
  }
}