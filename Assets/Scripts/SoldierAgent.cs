﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierAgent : MonoBehaviour, Agent
{
  public float fitness;

  private NeuralNetwork net;
  private int team;
  private List<SoldierAgent> teamMembers;
  private List<SoldierAgent> enemies;
  private bool isAlive = false;
  private int maxHealth = 100;
  private int currentHealth = 100;
  private Rigidbody2D rBody;
  private float speed = 2.0f;
  private int numberOfDistanceSensors = 9;
  private float fov = 100f;

  public void SetTeamMembers(List<SoldierAgent> soldiers)
  {
    this.teamMembers = soldiers;
  }

  public void SetEnemies(List<SoldierAgent> enemies)
  {
    this.enemies = enemies;
  }

  public bool IsAlive()
  {
    return isAlive;
  }

  public int Health()
  {
    return currentHealth;
  }

  public void Spawn(Vector3 position)
  {
    transform.position = position;
    gameObject.SetActive(true);
    fitness = 0f;
    currentHealth = maxHealth;
    isAlive = true;
  }

  public int Team()
  {
    return team;
  }

  public void SetTeam(int i)
  {
    team = i;
  }

  public float Fitness()
  {
    return fitness;
  }

  public void SetFitness(float fitness)
  {
    this.fitness = fitness;
  }

  public NeuralNetwork Network()
  {
    return net;
  }

  public void SetNetwork(NeuralNetwork newNet)
  {
    net = newNet;
  }

  public void Start()
  {
    name = UnityEngine.Random.Range(0, 100).ToString();
    net = new NeuralNetwork(new int[3] { enemies.Count * 3 + 1 + numberOfDistanceSensors, 5, 4 });
    rBody = GetComponent<Rigidbody2D>();
  }

  public void Init(int teamSize)
  {
    net = new NeuralNetwork(new int[3] { teamSize * 3 + 1 + numberOfDistanceSensors, 5, 4 });
  }
  public void FixedUpdate()
  {
    float[] inputs = GetInputs();
    float[] outputs = net.feedForward(inputs);
    Move(outputs[0], outputs[1], outputs[2]);
    Shoot(outputs[3]);
    fitness += rBody.velocity.magnitude;
  }

  private float[] GetInputs()
  {
    List<float> inputs = new List<float>();
    inputs.Add(transform.rotation.z);
    /*foreach (SoldierAgent member in teamMembers)
    {
      inputs.Add(member.Health() / maxHealth);
      inputs.Add(GetAngleTo(member.transform));
    }*/
    foreach (SoldierAgent enemy in enemies)
    {
      inputs.Add(GetAngleTo(enemy.transform));
    }
    foreach (SoldierAgent enemy in enemies)
    {
      RaycastHit2D[] hit = Physics2D.LinecastAll(transform.position, enemy.transform.position);
      if (hit.Length <= 1)
      {
        inputs.Add(0);
        continue;
      }
      var soldier = hit[1].collider.gameObject.GetComponent<SoldierAgent>();
      if (soldier && soldier.Team() != Team())
        inputs.Add(1);
      else
        inputs.Add(0);
    }
    foreach (SoldierAgent ally in teamMembers)
    {
      RaycastHit2D[] hit = Physics2D.LinecastAll(transform.position, ally.transform.position);
      if (hit.Length <= 1)
      {
        inputs.Add(0);
        continue;
      }
      var soldier = hit[1].collider.gameObject.GetComponent<SoldierAgent>();
      if (soldier && soldier.Team() == Team())
        inputs.Add(1);
      else
        inputs.Add(0);
    }
    var angleStep = fov / (numberOfDistanceSensors - 1);
    for (int i = 0; i < numberOfDistanceSensors; i++)
    {
      var vector = Quaternion.AngleAxis(angleStep * i + (90 - fov / 2), transform.forward) * -transform.up;
      //Debug.DrawRay(transform.position, vector * 100, Color.blue);
      RaycastHit2D[] hit = Physics2D.RaycastAll(transform.position, vector * 1000);
      if (hit.Length <= 1)
        inputs.Add(-1);
      else
        inputs.Add(hit[1].distance / 1000f);
    }
    return inputs.ToArray();
  }

  private float GetAngleTo(Transform t)
  {
    var localTarget = transform.InverseTransformPoint(t.position);
    var targetAngle = Mathf.Atan2(localTarget.x, localTarget.y) * Mathf.Rad2Deg;
    return targetAngle / 180f;
  }

  private void Move(float forwardBackward, float sideToSide, float turn)
  {
    if (forwardBackward > 0.3f)
      forwardBackward = 1;
    else if (forwardBackward < -0.3f)
      forwardBackward = -1;
    else
      forwardBackward = 0;
    if (sideToSide > 0.3f)
      sideToSide = 1;
    else if (sideToSide < -0.3f)
      sideToSide = -1;
    else
      sideToSide = 0;
    rBody.angularVelocity = turn * 400f;
    if (forwardBackward != 0 && sideToSide != 0)
      rBody.velocity = new Vector2(forwardBackward, sideToSide) * Mathf.Sqrt(2) * speed;
    else
      rBody.velocity = new Vector2(forwardBackward, sideToSide) * speed;
  }

  private void Shoot(float chance)
  {
    if (chance < 0)
      return;
    RaycastHit2D[] hit = Physics2D.LinecastAll(transform.position, transform.right * 1000);
    Debug.DrawRay(transform.position, transform.right * 100, Color.red);
    if (hit.Length < 2)
      return;
    var soldier = hit[1].collider.gameObject.GetComponent<SoldierAgent>();
    if (soldier)
    {
      soldier.DoDamage(1);
      if (soldier.Team() != Team())
      {
        // Debug.Log(name + " hit an enemy");
        //  Debug.Log("Hit enemy");
        fitness += 3000f;
      }
      else
      {
        //  Debug.Log("Hit teammate");
        // Debug.Log(name + " hit a teammate");
        fitness -= 5000f;
      }
    }
  }

  public void DoDamage(int damage)
  {
    currentHealth -= damage;
    if (currentHealth <= 0)
    {
      currentHealth = 0;
      isAlive = false;
      gameObject.SetActive(false);
    }
  }

  public void OnCollisionStay2D(Collision2D collision)
  {
    Debug.Log("Colliding with " + collision.gameObject.name);
    fitness -= 100f;
  }
}