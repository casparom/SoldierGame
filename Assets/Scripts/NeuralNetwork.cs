﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class NeuralNetwork
{
  public int[] layers;
  public float[][] neurons;
  public float[][][] weights;

  public NeuralNetwork(int[] layers)
  {
    this.layers = new int[layers.Length];
    for (int i = 0; i < layers.Length; i++)
      this.layers[i] = layers[i];
    initNeurons();
    initWeights();
  }

  public NeuralNetwork(NeuralNetwork net)
  {
    deepCopy(net);
  }

  public void deepCopy(NeuralNetwork net)
  {
    layers = new int[net.layers.Length];
    for (int i = 0; i < layers.Length; i++)
      layers[i] = net.layers[i];
    initNeurons();
    initWeights();
    for (int i = 0; i < weights.Length; i++)
      for (int j = 0; j < weights[i].Length; j++)
        for (int k = 0; k < weights[i][j].Length; k++)
          weights[i][j][k] = net.weights[i][j][k];
  }

  private void initNeurons()
  {
    List<float[]> neurons = new List<float[]>();
    for (int i = 0; i < layers.Length; i++)
      neurons.Add(new float[layers[i]]);
    this.neurons = neurons.ToArray();
  }

  private void initWeights()
  {
    List<float[][]> weights = new List<float[][]>();
    for (int i = 1; i < layers.Length; i++)
    {
      List<float[]> layerWeightList = new List<float[]>();
      for (int j = 0; j < neurons[i].Length; j++)
      {
        int neuronsInPreviousLayer = neurons[i - 1].Length;
        float[] neuronWeigths = new float[neuronsInPreviousLayer];
        for (int k = 0; k < neuronsInPreviousLayer; k++)
          neuronWeigths[k] = UnityEngine.Random.Range(-0.5f, 0.5f);
        layerWeightList.Add(neuronWeigths);
      }
      weights.Add(layerWeightList.ToArray());
    }
    this.weights = weights.ToArray();
  }

  public float[] feedForward(float[] inputs)
  {
    if (neurons[0].Length != inputs.Length)
    {
      Debug.Log("Error occured. Input length is not the same as the number of input neurons.");
      return null;
    }

    for (int i = 0; i < neurons[0].Length; i++)
      neurons[0][i] = inputs[i];

    for (int i = 1; i < layers.Length; i++)
      for (int j = 0; j < neurons[i].Length; j++)
      {
        float value = 0f;
        for (int k = 0; k < neurons[i - 1].Length; k++)
          value += neurons[i - 1][k] * weights[i - 1][j][k];
        neurons[i][j] = (float)Math.Tanh(value);
      }
    return neurons[layers.Length - 1];
  }

  public void mutate(float factor = 0.0f)
  {
    var resetChance = UnityEngine.Random.Range(0, 1000);
    if (resetChance < 1)
    {
      initWeights();
      return;
    }
    if (factor == 0.0f)
      factor = UnityEngine.Random.Range(0.0f, 5.0f);
    else if (factor > 5.0f)
      factor = 5.0f;
    for (int i = 1; i < weights.Length; i++)
      for (int j = 0; j < weights[i].Length; j++)
        for (int k = 0; k < weights[i][j].Length; k++)
        {
          float method = UnityEngine.Random.Range(factor - 5.0f, 1.0f);
          if (method >= 0.2f)
            weights[i][j][k] = UnityEngine.Random.Range(-0.5f, 0.5f);
          else if (method >= 0.4f)
            weights[i][j][k] *= -1f;
          else if (method >= 0.6f)
            weights[i][j][k] *= (UnityEngine.Random.Range(0f, 1f) + 1f);
          else if (method >= 0.8f)
            weights[i][j][k] *= UnityEngine.Random.Range(0f, 1f);
        }
  }
}