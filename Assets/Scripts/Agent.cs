﻿internal interface Agent
{
  int Team();

  void SetTeam(int i);

  float Fitness();

  void SetFitness(float fitness);

  NeuralNetwork Network();

  void SetNetwork(NeuralNetwork newNet);
}